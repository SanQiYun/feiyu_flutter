
<p align="center">
<img width="128" src="screenshots/logo.png" >
</p>
<p align="center">
飞鱼
<p>
<p align="center">
<img src="https://forthebadge.com/images/badges/built-with-love.svg">
<p>

# 简介 

飞鱼是使用Flutter开发的支援 #.m3u8, #.rtsp, #.rtmp 格式的视频播放器。

你可以在应用内添加符合 FeiFeiCms 接口规范的搜索源或者 #.dpl, #.m3u, #.xspf 格式的直播源。

同时，你也可以收藏自己中意的影片并制作成影单，记录自己的观影历史，不负好时光。

# 预览

**旧版** *（已开源）*

| ![](screenshots/1.jpg)  |  ![](screenshots/2.jpg)  |  ![](screenshots/3.jpg) |  ![](screenshots/4.jpg)  |
| :------------: | :------------: | :------------: | :------------: |

**新版**

| ![](screenshots/5.jpg)  |  ![](screenshots/6.jpg)  |  ![](screenshots/7.jpg) |  ![](screenshots/8.jpg)  |
| :------------: | :------------: | :------------: | :------------: |

# 免责声明

飞鱼为免费开源软件，仅供学习交流，用户可以非商业性地下载、安装、复制和散发本软件产品。 [免责声明](https://killer-1255480117.cos.ap-chongqing.myqcloud.com/feiyu_public/%E5%85%8D%E8%B4%A3%E5%A3%B0%E6%98%8E.html)

违规信息举报与反馈方式：请发邮件至 help@xbox.work，谢谢！


# 鸣谢


非常感谢以下开源项目，没有他们就没有本项目。

befovy / fijkplayer  *MIT License* 
https://github.com/befovy/fijkplayer/blob/master/LICENSE

succlz123 / DLNA-Dart *Apache License*
https://github.com/succlz123/DLNA-Dart/blob/master/LICENSE


IwantBEStrong / video_player_full_funciton 
https://github.com/IwantBEStrong/video_player_full_funciton
